// Place any global data in this file.
// You can import this data from anywhere in your site by using the `import` keyword.

export const SITE_TITLE = 'CISC-179 - Introduction to Python';
export const SITE_DESCRIPTION = 'Welcome to the Mesa College CISC-179 "Introduction to Python" course.';
